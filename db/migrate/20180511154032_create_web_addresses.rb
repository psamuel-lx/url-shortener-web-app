# @author Pedro Samuel <pedrosamuel.work@gmail.com>
# ActiveRecord::Migration > CreateWebAddresses
#
# file: db/migrations/20180511154032_create_web_addresses.rb
# table: web_addresses
# 
# Migration to create/remove web addresses table
class CreateWebAddresses < ActiveRecord::Migration[5.1]
  def change
    # Create web_addresses removing the primary field :id to create a custom primary key
    create_table :web_addresses, id: false do |t|
      # Custom primary key the save the random unique keys
      t.string :urid, primary_key: true, index: { unique: true }, null: false
      # Field for original url
      t.string :target, null: false
      # Field that saves the ip of the client that created the registration
      t.string :ip_origin, null: true
      # Field to save the last time the registration was accessed
      t.datetime :last_accessed_at, null: true

      t.timestamps
    end
  end
end
