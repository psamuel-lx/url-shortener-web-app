# URL Shortener - Web App (Runtime Revolution Exercise) #

Web application based on url shortener type like [Bitly](http://bitly.com/). 

Was developed for to correspond to the exercise of Runtime Revolution, that be checked in this [link](https://public.3.basecamp.com/p/hTAwfVBNuQuPVhGZqBfp3FWu).

Is a simple application and is not intended to replace any services that already exist.

## Setting up the URL SHORTENER WEBAPP (Local - Development Mode ONLY)  ##

### Requirements ###

* Git

* NodeJS

* Ruby - RBENV or RVM (Version ~ 2.3.1)

* Rails (Version >= 5.0.0)

* PostgreSQL (Version >= 9.5)

### Installing Ruby on Rails ###

This application was created by using Ruby on Rails, on the operating system Windows 10 with bash simulating the terminal of Ubuntu.

If you have don't have Ruby on Rails installed, please follow this guide to install Rails on MacOS, Windows or Ubuntu: [Install Ruby on Rails - Tutorial](https://gorails.com/setup/).
**But remember to install only the requirements** that were addressed before.

Note: The installed requirements need to be equal to the versions indicated above.

### Clone the Repository ###

Open the terminal and clone the repository to your local storage:

    # Create first the folders
    mkdir Development/RuntimeRevolution/

    # Open folder
    cd Development/RuntimeRevolution/
    
    # Clone the application
    git clone https://psamuel-lx@bitbucket.org/psamuel-lx/url-shortener-web-app.git

    # After the proccess is done, open the folder
    cd url-shortener-web-app

### Bundle GEMS ###

Install the necessary gems that are needed for the application. 

    # Inside the application folder use this command
    bundle install

Note: This requires that bundler gem is already installed.

### Database - PostgreSQL ###

Make sure you have PostgreSQL >= 9.5 correctly installed.

Change **DB_USER_NAME** and **DB_USER_PASSWORD** in config/application.yml file to your current credentials to access PostgreSQL (username and password).

Now to create database of the web application, follow the commands.

    # Create database (in the roots folder of the application)
    rake db:create
 
This will create the two databases, url_shortener_development and url_shortener_test.

    # Create the necessary tables
    rake db:migrate

### Final Steps ###

In the folder run the following command to run the local sever.

    rails server

Then open the brownser and use between this two urls http://localhost:3000 or http://lvh.me:3000 to run the web application on the brownser. =)

## Repository - Git ##

To commit changes run the commands.

    # Specify what you to commit
    git add [folder or files]

    # Describe the alterations that were made
    git commit -m "Description of the changes that were made"

    # Push the commit
    git push

To update the local application run the command.

    git pull

Or if you have a Git Client Tool installed use the actions that are explicit on the interface.

**Atenttion**

1. Don't make commits before warning your colleagues and verify with pulls first.

2. If you are not a developer don't do any alterations to the files and talk with responsibles on application development.

## Git-GUI Clients ##

### SourceTree - MacOS and Windows ###

  Install it at the official page - [Download SourceTree](https://www.sourcetreeapp.com/)
  Free to use.

### GitEye - Linux ###

  For Linux users you can use [GitEye](https://www.collab.net/products/giteye)
  Has a free license.

## Support ##

If there is any problem on executing the following steps to install the application locally. Comunicate with the responsibles of the development, make sure to share the errors in the [Repository Issues](https://bitbucket.org/psamuel-lx/url-shortener-web-app/issues).

## License ##

URL Shortener - Web App is an exercise made it for Runtime Revolution.

So it has no copyrights and the code can be used by everyone.
The use of this code for commercial bussines or other purposes are not addressed by any terms.

## Authors ##

The application was developed by Pedro Samuel for Runtime Revolution.