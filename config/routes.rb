Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "base#index"
  resources :web_addresses, only: :create
  get "/:urid", to: "web_addresses#show", as: :show
end
