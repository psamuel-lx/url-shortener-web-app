require "net/http"
module Validations
  extend ActiveSupport::Concern

  def validate_host(url)
    return if url.blank?
    url = uri_scematic!(url)
    !url.host.eql?(request.host) unless !url.host
  end
  
  def validate_url_existance(url)
    return if url.blank?
    url = uri_scematic!(url)
    net = Net::HTTP.new(url.host, url.port)
    req.use_ssl = url.scheme.eql?('https')
    path = url.path if url.path.present?
    res = req.request_head(path || '/')
    res.code.eql? "200"
  end

  def uri_scematic!(url)
    return if url.blank?
    uri = URI.parse(url.downcase)
    !uri.scheme ? URI.parse("http://#{url}") : uri  
  rescue URI::BadURIError
    false
  rescue URI::InvalidURIError
    false
  end

end