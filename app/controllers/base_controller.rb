# @author Pedro Samuel <pedrosamuel.work@gmail.com>
# ApplicationController > BaseController
#
# file: app/controllers/base_controller.rb
# url: <host_path>
# 
# Controller for primary usage
class BaseController < ApplicationController

  # Method that is used for the root path of the application
  # Handles with a GET request - <host_path>
  # 
  # @return [nil]
  def index
  end

end
