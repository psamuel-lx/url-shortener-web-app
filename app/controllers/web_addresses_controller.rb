# @author Pedro Samuel <pedrosamuel.work@gmail.com>
# ApplicationController > WebAddressesController
#
# file: app/controllers/web_addresses_controller.rb
# url: <host_path>/web_addresses
# 
# Controller to manage web addresses
class WebAddressesController < ApplicationController
  # Including concern Validations to be use the methods in this Controller
  # @note If you remove this concern can be effect the operations of this controller
  include Validations

  # Action to be executed before #show method
  # @see WebAddress#get_web_address method 
  before_action :get_web_address, only: :show

  # Method to get web_adddress object and redict user to target
  # Handles with a GET request - <host_path>/:urid
  # 
  # @param :urid [String] the reference to the object
  # @return [nil]
  def show
    # redirect user to the result path of the executed andle_redirect_path method
    # @see WebAddress#handle_redirect_path method 
    redirect_to handle_redirect_path
  end

  # Method to get web_adddress object and redict user to target
  # Handles with a POST request - <host_path>/web_addresses
  # 
  # @param :target [String] the web address to be shortened
  # @return [Hash<Key,Value>, nil] the result of the method create
  def create
    # Check if the request was a XMLHttpRequest and the format is of type JSON
    return unless request.xhr? && request.format.json?
    # Validate if host of the target is valid
    # @see Validations#validate_host(url[String])
    result = if validate_host(params[:target])
      # Remove protocol and subdomain to simplify the registration
      target = params[:target].downcase.gsub(/((https|http)?:\/\/)|(www\.)/, "")
      # Create web address if do not exist, otherwise return registration
      # Values used to create and find are :target and ip_origin
      @web_address = WebAddress.find_or_create_by(target: "http://#{target}", ip_origin: request.remote_ip)
      # If web address exist or was created then returns with shorter url, else return nil
      # The shorter url is the sum the host_path of the web appplication and the urid that references the original url
      [request.referer, @web_address.urid].join("") if @web_address.present?
    end
    # Respond using the following formats
    # @see ActionController::MimeResponds#respond_to
    respond_to do |format|
      format.json { render json: { result: result } }
    end
  end

  protected

  # Protected method used to redirect to original url if exists
  # 
  # @return [String] the path to be redirected 
  def handle_redirect_path
    # Check if web address exists
    if @web_address.present?
      # Update :last_accessed_at to have a notion the last time it was used
      @web_address.touch :last_accessed_at
      # returns original url
      @web_address.target
    else
      # returns root path if web address does not exist
      root_path
    end
  end

  private

  # Private method used to get the registration of the target url
  # 
  # @param :urid [String] the unique reference of the object
  # @return [Object] the object that has the registration of the target url
  def get_web_address
    @web_address = WebAddress.find_by_urid(params[:urid])
  end

end
