# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# Function that validates the url provided is a valid web addresss
# 
# param url [String] the provided link
# returns [Boolean] the result of the validation
validateURL = (url) ->
  # Check if parameter is blank or not
  if url == "" || typeof url == typeof undefined || url == null then return false
  # Using regex to simplify the condition
  urlregex = new RegExp("^(http|https)?:\/\/|(www.)|[a-zA-Z0-9-\.]+\.[a-z]{2,4}")
  # Test if it is valid
  urlregex.test(url)

# Toggles the navbar with a condition on depending where is situated in the screen
# 
# returns [NULL]
navbarCollapse = () ->
  if $("#mainNav").offset().top > 20 then $("#mainNav").addClass("navbar-shrink") else $("#mainNav").removeClass("navbar-shrink")

# Adds the possibility for user to press the Key "Enter" to execute the primary action
# 
# returns [NULL]
$(document).on "keyup", "input[type='text']", (event) ->
  if event.keyCode == 13 then $("button[type='button']").trigger("click")

# On changing behaviour of the input, removes the alerts if needed
# 
# returns [NULL]
$(document).on "change", "input[type='text']", (event) ->
  if $(this).hasClass("error") && $(this).val() != "" then $(this).removeClass("error")
  if $(this).parent().next(".alert").length != 0 then $(this).parent().next(".alert").remove()

# Trigger of the primary action to send data to the server to shorter
# the provided url in the textbox
#
# returns [NULL]
$(document).on "click", "button[type='button']", (event) ->
  # Validate if textbox exists
  if $(this).parent().prev("input").length != 0
    input = $(this).parent().prev("input")
    target = input.val()
    # Condition to validate if the url provided is valid
    # see function validateURL(url[String])
    unless validateURL(target)
      # If not valid warn user by sending error on textbox
      input.addClass("error")
    else
      # Else prepare to send data to the server using AJAX
      # 
      # param target [String] the provided url
      # returns [HASH[key, value]] the result of the request
      request = $.ajax
        url: "/web_addresses"
        type: 'POST'
        dataType: 'JSON'
        data:
          target: target
        beforeSend: () ->
          # Before requesting, disabled the textbox to not let insert another url before the result
          input.attr("disabled", true)
        success: (response) ->
          # Validates if the response is not null
          if response.result != null
            # If the condition is true, put the result on textbox by replacing the original url with short one
            input.val(response.result)
            input.addClass("success")
          else
            # Else warn user that something went wrong with the request
            # 1. The provided url is not valid or blank
            # 2. The server couldn't create the registration for the provided url
            input.addClass("error")
            input.parent().after("<div class='alert alert-danger'><strong>Error!</strong> Something went wrong. Please, verify if url provided is correct and try again.</div>")
        error: (jqXHR, textStatus) ->
          # If the request returns an error is because provided url is not valid or lost connection to the server
          input.addClass("error")
          input.parent().after("<div class='alert alert-danger'><strong>Error!</strong> Couldn't short the url.</div>")
        complete: (jqXHR, textStatus) ->
          # When completing the request whatever the result is, enable textbox
          input.attr("disabled", false)
          # And set a time to remove the alert
          setTimeout () ->
            input.parent().next(".alert").remove()
            input.removeClass("success")
          , 5000

$ ->
  
  # Collapse now if page is not at top
  navbarCollapse()

  # Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse)