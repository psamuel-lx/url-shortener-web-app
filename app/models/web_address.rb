# @author Pedro Samuel <pedrosamuel.work@gmail.com>
# ApplicationRecord > WebAddress
#
# file: app/model/web_address.rb
# table: web_addresses
# 
# Model that manage the web addresses registrations
class WebAddress < ApplicationRecord
  # Use custom primary key
  self.primary_key = "urid"

  # Validate if :urid is unique on create
  validates :urid, uniqueness: true, on: :create
  # Validate if :target is always present and if unique in base of :ip_origin 
  validates :target, presence: true, uniqueness: { case_sensitive: true, scope: :ip_origin }

  # Before create execute #make_urid mehtod
  # @see WebAddress#make_urid
  before_create :make_urid

  # Default scope ordered by :created_at for the first item to be accessed are the last ones that were created
  default_scope { order(created_at: :desc) }

  private

  # Private method to create a random and unique reference to the registration
  # 
  # @return [String] the random urid
  def make_urid
    self.urid = loop do
      random_urid = SecureRandom.urlsafe_base64(7)
      break random_urid unless WebAddress.exists?(urid: random_urid)
    end
  end

end
